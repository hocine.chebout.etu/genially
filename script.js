let user = {
    pseudo: "joueur",
    localisation: "menue",
}


function setDisplay(zone,display="none") {
    var div = document.getElementById(zone);
    div.style.display = display;
}


function getPseudo(event) {
    let key = event.code;
    if (key == "Enter"){
        var input = document.getElementById("input-pseudo").value;
        user.pseudo = input;
        user.localisation = "scene-1-1"
        setDisplay("pseudo");
        setDisplay("image-scene-1-1","block");
        setDisplay("editeur","grid");
    }
}

function lancer() {
    setDisplay("menu");
    setDisplay("pseudo","block");
}

function changer() {
    if (user.localisation == "scene-1-1"){
        user.localisation = "scene-1-2"
        setDisplay("image-scene-1-1");
        setDisplay("image-scene-1-2","block");
        setDisplay("mdp-scene-1","block");
    }
    else if (user.localisation== "scene-1-2"){
        user.localisation = "scene-2";
        setDisplay("image-scene-1-1");
        setDisplay("image-scene-1-2");
        setDisplay("editeur-scene-1");
        setDisplay("mdp-scene-1");
        setDisplay("manual-scene-1");

        setDisplay("image-scene-2","block");
        setDisplay("scene-2","block");
        setDisplay("texte-scene-2","block");
    }
}

function ordinateur() {
    setDisplay("ordi","block");
    setDisplay("editeur-scene-2","block");
    setDisplay("mdp-scene-2","block");

    setDisplay("image-scene-2");
    setDisplay("texte-scene-2");
}

function manual() {
    if (document.getElementById("titre-f").textContent == "editeur"){
        document.getElementById("titre-f").innerHTML = "manuel";
        if (user.localisation == "scene-1-1" || user.localisation == "scene-1-2"){
            setDisplay("manual-scene-1","block");
            setDisplay("cible-scene-1","block");
            setDisplay("editeur-scene-1");
        }
        else if (user.localisation == "scene-2"){
            setDisplay("manual-scene-2","block");
            setDisplay("editeur-scene-2");
        }
    }
    else {
        document.getElementById("titre-f").innerHTML = "editeur";
        if (user.localisation == "scene-1-1" || user.localisation == "scene-1-2"){
            setDisplay("editeur-scene-1","block");
            setDisplay("manual-scene-1");
        }
        else if (user.localisation == "scene-2"){
            console.log("cc");
            setDisplay("manual-scene-2");
            setDisplay("editeur-scene-2","block");
        }
    }

}

function getMdp() {
    var input="";
    if (user.localisation == "scene-1-2"){
        input = document.getElementById("mdp-scene-1").value;
		if (input == "la porte"){
            changer();
        }
    }
    if (user.localisation == "scene-2"){
        input = document.getElementById("mdp-scene-2").value;
        console.log(input);
    }
}

var cacher = false;

function reduire(){
    if (!cacher){
        var div = document.getElementById("content");
        div.style.display = "none";
        cacher = true;
    }
    else {
        var div = document.getElementById("content");
        div.style.display = "block";
        cacher = false;
    }
}

dragElement(document.getElementById("editeur"));

function dragElement(elmnt) {
    var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
    if (document.getElementById(elmnt.id + "Header")) {
        document.getElementById(elmnt.id + "Header").onmousedown = dragMouseDown;
    } else {
        elmnt.onmousedown = dragMouseDown;
    }

    function dragMouseDown(e) {
        e = e || window.event;
        e.preventDefault();
        pos3 = e.clientX;
        pos4 = e.clientY;
        document.onmouseup = closeDragElement;
        document.onmousemove = elementDrag;
    }

    function elementDrag(e) {
        e = e || window.event;
        e.preventDefault();
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;
        elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
        elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
    }

    function closeDragElement() {
        document.onmouseup = null;
        document.onmousemove = null;
    }
}


function getValue(event,zone) {
    let key = event.code;
    if (key == "Enter"){
        if (zone == "color-scene-1"){
            var input = document.getElementById("color-scene-1").value;
            console.log(input);
            if (input.substring(input.length -1) == ';'){
                var div = document.getElementById('cible-scene-1');
                console.log(div.style.color);
                div.style.color = ""+input.substring(0,input.length -1);
            }
        }

        if (zone == "couleur-scene-2"){
            var input = document.getElementById("couleur-scene-2").value;
            if (input.substring(input.length -1) == ';'){
                var div = document.getElementById('cible-scene-2');
                div.style.color = ""+input.substring(0,input.length -1);
            }
        }
        if (zone == "display-scene-2"){
            var input = document.getElementById("display-scene-2").value;
            if (input.substring(input.length -1) == ';'){
                var div = document.getElementById('cible-scene-2');
                div.style.display = ""+input.substring(0,input.length -1);
            }
        }
    }
}
